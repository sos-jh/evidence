package evidence;

/**
 * Created by martin on 27. 9. 2014.
 */
public class Tractor extends Car
{
    public Tractor(String id, String model, String color) {
        super(id, model, color);
        super.setMaxSpeed( 50 );
    }

    public Tractor(String id, String model) {
        super(id, model);
        super.setMaxSpeed( 50 );
    }
}
