package evidence;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by martin on 27. 9. 2014.
 */
public class Evidence
{
    private List<Employee>  employees   =   new ArrayList<>();
    private List<Driver>    drivers     =   new ArrayList<>();
    private List<Car>       cars        =   new ArrayList<>();

    public void addEmployee( Employee employee ) throws Exception
    {
        if( this.employees.contains( employee ) )
        {
            throw new Exception( "zamestnanec uz je v evidenci" );
        }

        this.employees.add( employee );
    }

    public void addDriver( Driver driver ) throws Exception
    {
        if( this.drivers.contains( driver ) )
        {
            throw new Exception( "ridic uz je v evidenci" );
        }

        this.drivers.add( driver );
    }

    public void addCar( Car car ) throws Exception
    {
        if( this.cars.contains( car ) )
        {
            throw new Exception( "auto uz je v evidenci" );
        }

        this.cars.add( car );
    }

    public static void main(String[] args) throws Exception
    {
        Employee    e1  =   new Employee( "Omakalamiho" , "Nakanapi" , "marketing" );
        Employee    e2  =   new Employee( "Masemeno" , "Nasaku" , "it" );

        e1.setBirthDate( new GregorianCalendar( 1969 , 12 , 15 ) );

        Driver      d1  =   new Driver( "Mamji" , "Jakojamu" , DriverLicense.B1 );
        Driver      d2  =   new Driver( "Nahmád" , "Ohanbí" );

        d2.setLicense( DriverLicense.D1E );

        Car     c1  =   new Car( "ABC1234" , "škoda fabia" , "zelená" );
        Car     c2  =   new Car( "DEF1234" , "fiat punto" );
        Truck   tu1 =   new Truck( "GHJ1234" , "liaz" , 6 );
        Tractor tr1 =   new Tractor( "ZZZ666" , "john deere" , "modrá" );
//        Truck   t0  =   new Truck( "GHJ1234" , "liaz" , 18 );

        c2.setColor( "hnědá" );
        c2.setMaxSpeed( 140 );

        d1.addCar( c1 );
        d1.addCar( tr1 );
//        d1.addCar( tr1 );

        Evidence    evd =   new Evidence();

        evd.addCar( c1 );
        evd.addCar( c2 );
        evd.addCar( tu1 );
        evd.addCar( tr1 );
        evd.addDriver( d1 );
        evd.addDriver( d2 );
        evd.addEmployee( e1 );
        evd.addEmployee( e2 );
//        evd.addEmployee( e2 );
//        evd.addDriver( d2 );
//        evd.addCar( c1 );

        System.out.println( c2.getMaxSpeed() );
        System.out.println( tr1.getMaxSpeed() );
        System.out.println( tu1.getMaxSpeed() );
        System.out.println( tu1.getAxles() );

        System.out.println( evd.findDriver( "Mamji Jakojamu" ) );
        System.out.println( evd.findEmployee( "Omakalamiho Nakanapi" ) );
        System.out.println( evd.findEmployee( "Maho Jakotyč" ) );
        System.out.println( evd.findCar( "ZZZ666" ) );
    }

    public Driver findDriver( String name )
    {
        return (Driver)this._find( new ArrayList<Person>( this.drivers ) , name );

    }

    public Employee findEmployee( String name )
    {
        return (Employee)this._find( new ArrayList<Person>( this.employees ) , name );
    }

    private Person _find( List<Person> persons , String name )
    {
        String _name[]  =   name.split( " " );

        for( Person p : persons )
        {
            if( p.getFirstName().equals( _name[0] ) && p.getLastName().equals( _name[1] ) )
            {
                return p;
            }
        }

        return null;
    }

    public Car findCar( String id )
    {
        for( Car c : this.cars )
        {
            if( c.getId().equals( id ) )
            {
                return c;
            }
        }

        return null;
    }
}
