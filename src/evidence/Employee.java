package evidence;

import java.util.Calendar;

/**
 * Created by martin on 27. 9. 2014.
 */
public class Employee extends Person
{
    private String  department;

    /**
     *
     * @param firstName
     * @param lastName
     * @param department
     */
    public Employee( String firstName , String lastName , String department )
    {
        super( firstName, lastName );
        this.setDepartment( department );
    }

    /**
     *
     * @param firstName
     * @param lastName
     * @param birthDate
     */
    public Employee(String firstName, String lastName, Calendar birthDate) {
        super(firstName, lastName, birthDate);
    }

    public void setDepartment( String department )
    {
        this.department =   department;
    }

    public String getDepartment()
    {
        return this.department;
    }

    public String toString()
    {
        return super.toString() + " - " + this.getDepartment();
    }
}
