package evidence;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by martin on 27. 9. 2014.
 */
public class Driver extends Person
{
    private DriverLicense   license;

    private List<Car>       cars    =   new ArrayList<>();

    /**
     *
     * @param firstName
     * @param lastName
     * @param license
     */
    public Driver(String firstName, String lastName, DriverLicense license)
    {
        super(firstName, lastName);
        this.setLicense( license );
    }

    /**
     *
     *
     * @param firstName
     * @param lastName
     */
    public Driver(String firstName, String lastName) {
        super(firstName, lastName);
    }

    public DriverLicense getLicense()
    {
        return this.license;
    }

    public void setLicense( DriverLicense license )
    {
        this.license    =   license;
    }

    public void addCar( Car car ) throws Exception
    {
        if( this.cars.contains( car ) )
        {
            throw new Exception( "toto auto jiz ma prirazeno" );
        }
        else
        {
            this.cars.add( car );
        }
    }

    public void removeCar( Car car ) throws Exception
    {
        if( this.cars.contains( car ) )
        {
            this.cars.remove( car );
        }
        else
        {
            throw new Exception( "toto auto nema prirazeno" );
        }
    }

    public boolean ownsCar( Car car )
    {
        return this.cars.contains( car );
    }

    public List<Car> getCars()
    {
        return this.cars;
    }

    public String toString()
    {
        return super.toString() + " - " + this.cars.size();
    }
}
