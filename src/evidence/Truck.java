package evidence;

/**
 * Created by martin on 27. 9. 2014.
 */
public class Truck extends Car
{
    private int axles;

    public Truck(String id, String model, String color) {
        super(id, model, color);
    }

    public Truck( String id , String model , int axles ) throws Exception
    {
        super(id, model);

        if( !( axles > 4 && axles <= 12 ) )
        {
            throw new Exception( "pocet naprav musi byt mezi 4 a 12" );
        }

        this.axles  =   axles;
    }

    public int getAxles()
    {
        return this.axles;
    }
}
