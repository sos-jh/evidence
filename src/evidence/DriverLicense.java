package evidence;

/**
 * Created by martin on 27. 9. 2014.
 */
public enum DriverLicense
{
    AM( "Motorová vozidla s konstrukční rychlostí nepřevyšující 45 km/h. U dvoukolových vozidel je příslušnost omezena zdvihovým objemem válců spalovacího motoru nepřevyšujícím 50 ccm a výkonem elektrického motoru do 4 kW, u tříkolových a čtyřkolových vozidel se shodné omezení kubatury vztahuje pouze na zážehové motory a omezení výkonem 4 kW na všechny ostatní typy motorů. U čtyřkolových vozidel je navíc příslušnost ke skupině omezena hmotností vozidla v nenaloženém stavu nejvýše 350 kg. K řízení vozidel skupiny AM opravňuje kterékoliv řidičské oprávnění s výjimkou oprávnění pro skupinu T." , 15 ),
    A1( "Lehké motocykly s postranním vozíkem i bez něj, o výkonu nejvýše 11 kW a s poměrem výkonu/hmotnosti nejvýše 0,1 kW/kg a se zdvihovým objemem spalovacího motoru nepřevyšujícím 125 ccm.\n"+
                "Tříkolová motorová vozidla o výkonu nejvýše 15 kW\n"+
                "Řidičské oprávnění pro skupinu A1 platí i pro čtyřkolová vozidla o výkonu motoru do 15 kW, s hmotností v nenaloženém stavu do 400 kg a se zdvihovým objemem spalovacího motoru do 125 ccm." , 16 ),
    A2( "Motocykly s postranním vozíkem i bez něj s výkonem motoru nejvýše 35 kW a s poměrem výkonu/hmotnosti nejvýše 0,2 kW/kg, které nebyly upraveny z motocyklu s více než dvojnásobným výkonem. Řidičské oprávnění pro skupinu A2 platí i pro vozidla skupiny A1." , 18 ) ,
    A( "Motocykly s postranním vozíkem nebo bez něj.\n" +
            "Tříkolová motorová vozidla nespadající do skupiny A1 (tj. s vyšším výkonem než 15 kW).\n" +
            "Řidičské oprávnění pro skupinu A platí i pro vozidla skupin A2 a A1.\n" +
            "Řidičské oprávnění pro skupinu A opravňuje také k řízení čtyřkolových vozidel o výkonu motoru do 15 kW a hmotnosti v nenaloženém stavu do 400 kg." , 24 ) ,
    B1( "Čtyřkolová motorová vozidla nespadající do skupiny AM, jejichž výkon nepřevyšuje 15 kW a hmotnost v nenaloženém stavu nepřevyšuje 400 kg nebo 550 kg u vozidel určených k přepravě zboží. Řidičské oprávnění pro skupinu B1 platí i pro tříkolová vozidla ze skupiny A1." , 17 ) ,
    B( "Motorová vozidla, která nespadají do žádné ze skupin AM, A1, A2, A a B1, s největší povolenou hmotností nepřevyšující 3,5 tuny a maximálně 8 místy k sezení mimo řidiče, ke kterým smí být připojeno přípojné vozidlo:\n" +
            "a) o největší povolené hmotnosti nepřevyšující 750 kg\n" +
            "\n" +
            "b) o největší povolené hmotnosti převyšující 750 kg, pokud největší povolená hmotnost této jízdní soupravy nepřevyšuje 3 500 kg (Bylo mi policií vysvětleno že toto neplatí)\n" +
            "\n" +
            "c) o největší povolené hmotnosti převyšující 750 kg, pokud největší povolená hmotnost této jízdní soupravy nepřevyšuje 4 250 kg, jedná-li se o řidičské oprávnění skupiny B v rozšířeném rozsahu (harmonizační kód 96).\n" +
            "\n" +
            "Řidičské oprávnění skupiny B platí i pro vozidla skupiny A1 s automatickou převodovkou a pro vozidla skupiny B1. Řidičské oprávnění skupiny B opravňuje držitele, který dosáhl věku 21 let, i k řízení tříkolových vozidel skupiny A." , 18 ) ,
    BE( "Jízdní soupravy z motorového vozidla skupiny B a přípojného vozidla těžšího 750 kg, celková hmotnost jízdní soupravy může překročit 3,5 tuny, ale nesmí překočit 7 tun. Lze jej udělit jen držiteli oprávnění skupiny B." , 18 ) ,
    C1( "Motorová vozidla, s výjimkou traktorů, o maximální přípustné hmotnosti převyšující 3,5 tuny a nepřevyšující 7,5 tuny, s maximálně 8 místy k sezení mimo místa řidiče. K vozidlu může být připojeno přípojné vozidlo o nejvšší povolené hmotnosti do 750 kg. Lze jej udělit jen držiteli oprávnění skupiny B." , 18 ) ,
    C1E( "Jízdní soupravy o nejvyšší povolené hmotnosti nepřevyšující 12 tun složené buď z motorového vozidla skupiny C1 a přípojného vozidla o největší povolené hmotnosti převyšující 750 kg nebo z motorového vozidla skupiny B a přípojného vozidla o největší povolené hmotnosti převyšující 3,5 tuny. Řidičské oprávnění skupiny C1+E opravňuje i k řízení jízdních souprav spadajících do skupiny B+E. Lze jej udělit jen držiteli oprávnění skupiny C1." , 18 ) ,
    C( "Motorová vozidla s výjimkou traktorů o maximální přípustné hmotnosti převyšující 3,5 tuny a maximálně 8 místy k sezení mimo místa řidiče. K vozidlu může být připojeno přípojné vozidlo o nejvyšší povolené hmotnosti do 750 kg. Řidičské oprávnění skupiny C opravňuje i k řízení vozidel skupiny C1. Lze jej udělit jen držiteli oprávnění skupiny B." , 21 ) ,
    CE( "Jízdní soupravy složené z motorového vozidla skupiny C a přípojného vozidla, pokud nejde o jízdní soupravu, která celá spadá pod skupinu C. Řidičské oprávnění skupiny C+E opravňuje i k řízení jízdních souprav spadajících do skupiny B+E nebo C1+E. Řidičské oprávnění skupiny C+E ve spojení s řidičským oprávněním skupiny D platí i pro jízdní soupravy spadající do skupiny D+E. Lze jej udělit jen držiteli oprávnění skupiny C." , 21 ) ,
    D1( "Motorová vozidla o délce nepřesahující 8 metrů s více než 8, ale nejvíce 16 místy k sezení, kromě místa řidiče, k tomuto vozidlu smí být připojeno přípojné vozidlo, jehož největší povolená hmotnost nepřevyšuje 750 kg. Lze jej udělit jen držiteli oprávnění skupiny B." , 21 ) ,
    D1E( "Jízdní soupravy složené z motorového vozidla spadajícího pod skupinu D1 a přípojného vozidla, pokud nejde o jízdní soupravu, která celá spadá pod skupinu D1. Řidičské oprávnění skupiny D1+E opravňuje i k řízení jízdních souprav spadajících do skupiny B+E. Lze jej udělit jen držiteli oprávnění skupiny D1." , 21 ) ,
    D( "Motorová vozidla nespadající do skupiny D1 s více než 8 místy k sezení, kromě místa řidiče, k tomuto vozidlu smí být připojeno přípojné vozidlo, jehož maximální přípustná hmotnost nepřevyšuje 750 kg. Řidičské oprávnění skupiny D platí i pro vozidla skupiny D1. Lze jej udělit jen držiteli oprávnění skupiny B. Řidičské oprávněné skupiny D je vyžadováno i k řízení trolejbusu, vedle oprávnění k řízení drážního vozidla." , 24 ) ,
    DE( "Jízdní soupravy složené z motorového vozidla spadajícího pod skupinu D a přípojného vozidla, pokud nejde o jízdní soupravu, která celá spadá pod skupinu D. Řidičské oprávnění skupiny D+E opravňuje i k řízení jízdních souprav spadajících do skupiny B+E nebo D1+E. Lze jej udělit jen držiteli oprávnění skupiny D." , 24 ) ,
    T( "Traktory a samojízdné pracovní stroje, a to i s přípojným vozidlem." , 17 );

    private String  description;
    private int     age;

    private DriverLicense( String description , Integer age )
    {
        this.description    =   description;
        this.age            =   age;
    }

    public int getMinimalAge()
    {
        return this.age;
    }

    public String getDescription()
    {
        return this.description;
    }
}