package evidence;

import java.util.Calendar;

/**
 * Created by martin on 27. 9. 2014.
 */
abstract public class Person
{
    private String      firstName;
    private String      lastName;
    private Calendar    birthDate;

    public Person( String firstName , String lastName , Calendar birthDate )
    {
        this( firstName , lastName );
        this.setBirthDate( birthDate );
    }

    public Person( String firstName , String lastName )
    {
        this.firstName  =   firstName;
        this.lastName   =   lastName;
    }

    public String getFirstName()
    {
        return this.firstName;
    }

    public String getLastName()
    {
        return this.lastName;
    }

    public Calendar getBirthDate()
    {
        return this.birthDate;
    }

    public void setBirthDate( Calendar birthDate )
    {
        this.birthDate  =   birthDate;
    }

    public String toString()
    {
        return this.getFirstName() + " " + this.getLastName() + " "
                + ( null == this.birthDate ? "" : String.format("%1$te. %1$tm. %1$tY", this.birthDate ) );
    }
}
