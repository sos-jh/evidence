package evidence;

/**
 * Created by martin on 27. 9. 2014.
 */
public class Car
{
    private String  id;
    private String  model;
    private String  color;
    protected Integer   maxSpeed;

    /**
     *
     * @param id
     * @param model
     * @param color
     */
    public Car( String id , String model , String color )
    {
        this( id , model );
        this.setColor( color );
    }

    /**
     *
     * @param id
     * @param model
     */
    public Car( String id , String model )
    {
        this.id     =   id;
        this.model  =   model;
    }

    public String getColor()
    {
        return this.color;
    }

    public void setColor( String color )
    {
        this.color  =   color;
    }

    public String getId()
    {
        return this.id;
    }

    public String getModel()
    {
        return this.model;
    }

    public Integer getMaxSpeed()
    {
        return this.maxSpeed;
    }

    public void setMaxSpeed( Integer maxSpeed )
    {
        this.maxSpeed   =   maxSpeed;
    }

    public String toString()
    {
        return this.getId() + " : " + this.getModel() + " : " + this.getColor();
    }
}
